<?php /* $Id$ */?>

<?php
/**
 * Available variables:
 * $type
 *   The type of template being displayed. Either TAXONOMY_PAGES_DEFAULT_DISPLAY, TAXONOMY_PAGES_TAXONOMY_TERM_DISPLAY, or TAXONOMY_PAGES_NO_DISPLAY.
 * Case TAXONOMY_PAGES_DEFAULT_DISPLAY:
 * $nodes
 *   An array of nodes, ready-to-be-displayed (pre-themed).
 * $term
 *   A term object, full of data that can be displayed.
 * $tid
 *   The term id of the term. Usually not very useful.
 * Case TAXONOMY_PAGES_TAXONOMY_TERM_DISPLAY:
 * $output
 *   The output generated from the page in question.
 */
?>
<?php if ($type == TAXONOMY_PAGES_DEFAULT_DISPLAY): ?>
  <?php if (!empty($nodes)): ?>
    <hr>
    <div class="description">
      This is a <?php print theme('placeholder', 'category page') ?>. Listed below is the content associated with this category.
    </div>
    <h2>
      <?php print ucfirst($term->name); ?>
    </h2>
    <div class="description">
      <?php print $term->description; ?>
    </div>
    <div class="taxonomy-pages-page">
      <?php foreach ($nodes as $node): ?>
        <div class="taxonomy-pages-node">
          <?php print $node; ?>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
<?php endif; ?>

<?php if ($type == TAXONOMY_PAGES_TAXONOMY_TERM_DISPLAY): ?>
  <hr>
  <h2>
    <?php print ucfirst($term->name); ?>
  </h2>
  <?php print $output; ?>
<?php endif; ?>