<?php

/**
 * Menu callback: admin settings.
 */
function taxonomy_pages_admin_settings() {
  $form = array();
  $form['taxonomy_pages_display_type'] = array(
    '#type' => 'radios',
    '#title' => t('Display type'),
    '#options' => array(
      TAXONOMY_PAGES_DEFAULT_DISPLAY => t('Default display'),
      TAXONOMY_PAGES_TAXONOMY_TERM_DISPLAY => t('Display exactly as taxonomy term page for this term would display.'),
      TAXONOMY_PAGES_NO_DISPLAY => t('Do not display'),
    ),
    '#default_value' => variable_get('taxonomy_pages_display_type', TAXONOMY_PAGES_DEFAULT_DISPLAY),
  );
  $form['taxonomy_pages_taxonomy_term_action'] = array(
    '#type' => 'radios',
    '#title' => t('Action on taxonomy term page'),
    '#description' => t('What to do when one goes to a taxonomy term page without a current category page'),
    '#options' => array(
      TAXONOMY_PAGES_NO_ACTION => t('Do not do anything: display the normal page.'),
      TAXONOMY_PAGES_PAGE_NOT_FOUND => t('Return a <em>page not found</em> error.'),
      TAXONOMY_PAGES_CREATION_LINK => t('Display a link to create the taxonomy page if the user has access.'),
    ),
    '#default_value' => variable_get('taxonomy_pages_taxonomy_term_action', TAXONOMY_PAGES_NO_ACTION),
  );
  return system_settings_form($form);
}