<?php

// Definitions

// Display constants.
define('TAXONOMY_PAGES_DEFAULT_DISPLAY', 0);
define('TAXONOMY_PAGES_TAXONOMY_TERM_DISPLAY', 1);
define('TAXONOMY_PAGES_NO_DISPLAY', 2);

// Taxonomy term page constants.
define('TAXONOMY_PAGES_NO_ACTION', 0);
define('TAXONOMY_PAGES_PAGE_NOT_FOUND', 1);
define('TAXONOMY_PAGES_CREATION_LINK', 2);

/**
 * Start of API functions.
 * These will not change without notice.
 */

/**
 * API function: fetch the tid based on the nid.
 * @param $nid
 *   The nid of the tid to load.
 * @return
 *   The tid fetched.
 */
function taxonomy_pages_get_tid($nid) {
  return taxonomy_pages_fetch_data($nid, 'nid', FALSE);
}

/**
 * API function: fetch the term based on the nid.
 * @param $nid
 *   The nid of the term to load.
 * @return
 *   The full term object fetched.
 */
function taxonomy_pages_get_term($nid) {
  return taxonomy_pages_fetch_data($nid, 'nid', TRUE);
}

/**
 * API function: fetch the nid based on the tid.
 * @param $tid
 *   The tid of the nid to load.
 * @return
 *   The nid fetched.
 */
function taxonomy_pages_get_nid($tid) {
  return taxonomy_pages_fetch_data($tid, 'tid', FALSE);
}

/**
 * API function: fetch the node based on the tid.
 * @param $tid
 *   The tid of the node to load.
 * @return
 *   The nid fetched.
 */
function taxonomy_pages_get_node($tid) {
  return taxonomy_pages_fetch_data($tid, 'tid', TRUE);
}

/**
 * The main function for all the API callbacks.
 * This is within its own function for performance gains.
 * @param $id
 *   The id of the data to load, either nid or tid.
 * @param $type
 *   The type of the data being passed in, either 'nid' or 'tid'.
 *   Determines what type of id $id is.
 * @parram $full
 *   Boolean: true if the full term/node should be loaded, false otherwise.
 * @return
 *   Either tid, term, nid, or node.
 */
function taxonomy_pages_fetch_data($id, $type, $full) {
  // Cache the data.
  static $nids = array();
  // First, check the cache.
  switch ($type) {
    case 'nid':
      if (isset($nids[$id])) {
        return $nids[$id];
      }
      $other = 'tid';
      break;

    case 'tid':
      // We don't need to check for it being a boolean because the value here will never be empty() anyway.
      if ($key = array_search($id, $nids)) {
        return $key;
      }
      $other = 'nid';
      break;
  }

  // If that failed, fetch the data.
  $data = db_result(db_query('SELECT %s FROM {taxonomy_pages} WHERE %s = %d', $other, $type, $id));
  if (empty($data)) {
    // Operation failed.
    return FALSE;
  }

  switch ($type) {
    case 'nid':
      $nids[$id] = $data;
      if ($full) {
        $data = taxonomy_get_term($data);
      }
      break;

    case 'tid':
      $nids[$data] = $id;
      if ($full) {
        $data = node_load($data);
      }
      break;
  }
  return $data;
}
/**
 * End of API functions.
 * Everything below is subject to change without notice.
 */

/**
 * Implementation of hook_perm().
 */
function taxonomy_pages_perm() {
  foreach (node_get_types() as $type) {
    $perms[] = 'assign '. $type->type .' nodes to terms';
  }
  return $perms;
}

/**
 * Implementation of hook_menu().
 */
function taxonomy_pages_menu() {
  $items = array();
  $items['admin/content/taxonomy/taxonomy_pages'] = array(
    'title' => 'Taxonomy pages',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('taxonomy_pages_admin_settings'),
    'file' => 'taxonomy_pages.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => -8,
  );
  return $items;
}

/**
 * Implementation of hook_form_alter().
 */
function taxonomy_pages_form_alter(&$form, $form_state, $form_id) {
  // Vocabulary admin form.
  if ($form_id == 'taxonomy_form_vocabulary') {
    $checked = FALSE;
    $form['taxonomy_pages_update'] = array('#type' => 'value', '#value' => FALSE);
    if (isset($form['vid']['#value'])) {
      $in = db_result(db_query('SELECT vid FROM {taxonomy_pages_vocabulary} WHERE vid = %d', $form['vid']['#value']));
      if (!empty($in)) {
        $form['taxonomy_pages_update']['#value'] = TRUE;
        $checked = TRUE;
      }
    }
    // Add our own checkbox to the form. Double nest it to allow for other modules to group stuff.
    $form['settings']['taxonomy_pages']['taxonomy_pages'] = array(
      '#type' => 'checkbox',
      '#title' => t('Taxonomy pages'),
      '#default_value' => $checked,
      '#description' => t('Should this vocabulary\'s pages be handled by the taxonomy pages module?'),
    );
    // Add our own submit function to process the value.
    $form['#submit'][] = 'taxonomy_pages_vocabulary_form_submit';
  }

  // Term edit form
  if ($form_id == 'taxonomy_form_term') {
    if (!is_null($form['#term']['tid'])) {
      $node = taxonomy_pages_get_node($form['#term']['tid']);
      if (!empty($node->nid)) {
        $form['identification']['taxonomy_pages_link'] = array(
          '#type' => 'markup',
          '#value' => '<p></p>'. t('<strong>Taxonomy page: </strong><a href=@url>%page</a>', array('@url' => url('node/'. $node->nid), '%page' => $node->title)) .'<p></p>',
          '#weight' => -1,
        );
      }
    }
  }

  // Node form.
  if (isset($form['type']) && isset($form['#node']) && $form['type']['#value'] .'_node_form' == $form_id) {
    // Check to make sure the user has access.
    if (user_access('assign '. $form['type']['#value'] .' nodes to terms')) {
      $node = $form['#node'];
      $query = db_query('SELECT vid FROM {taxonomy_pages_vocabulary}');
      $vids = array(); 
      while ($result = db_result($query)) {
        $vids[] = $result;
      }
      if (!empty($vids)) {
        $form['taxonomy_pages'] = array('#type' => 'fieldset', '#tree' => TRUE, '#title' => t('Taxonomy pages'), '#collapsible' => TRUE, '#collapsed' => TRUE);
      }
      foreach ($vids as $vid) {
        $terms = array();
        if (!empty($_GET['taxonomy_pages'])) {
          if (is_numeric($_GET['taxonomy_pages'])) {
            $node->taxonomy_pages = $_GET['taxonomy_pages'];
          }
        }
        $taxonomy_form = taxonomy_form($vid, !empty($node->taxonomy_pages) ? array($node->taxonomy_pages) : array(), t('If this is a taxonomy page, select the taxonomy term to which it belongs.'));
        if ($select = taxonomy_pages_filter_out_terms($taxonomy_form, (empty($node->taxonomy_pages) ? NULL : $node->taxonomy_pages))) {
          $form['taxonomy_pages'][$vid] = $select;
        }
      }
    }

  }
  // We do not need to return anything because $form was passed in by reference.
}

/**
 * Filters out all terms that already have a page.
 */
function taxonomy_pages_filter_out_terms($form, $default) {
  $query = db_query('SELECT tid FROM {taxonomy_pages}');
  $disallowed = array();
  while ($tid = db_result($query)) {
    $disallowed[$tid] = $tid;
  }

  $terms = $form['#options'];
  foreach ($terms as $key => $term) {
    if (is_object($term)) {
      foreach ($term->option as $tid => $label) {
        if (isset($disallowed[$tid])) {
          if ($tid != $default) {
            unset($terms[$key]);
          }
          else {
            $terms[$key]->option[$tid] .= t(' (default)');
          }
        }
      }
    }
  }
  $form['#options'] = $terms;
  return $form;
}
/**
 * Submit callback.
 */
function taxonomy_pages_vocabulary_form_submit($form, &$form_state) {
  if ($form_state['values']['taxonomy_pages'] && !$form_state['values']['taxonomy_pages_update']) {
    $vid = $form_state['values']['vid'];
    $query = array('vid' => $vid);
    drupal_write_record('taxonomy_pages_vocabulary', $query);
  }
  elseif (!$form_state['values']['taxonomy_pages'] && $form_state['values']['taxonomy_pages_update']) {
    db_query('DELETE FROM {taxonomy_pages_vocabulary} WHERE vid = %d', $form_state['values']['vid']);
  }
}

/**
 * Implementation of hook_link_alter().
 * We alter the links here and not in hook_term_path because we cannot set $vocab['module'] = taxonomy_pages
 * because we want taxonomy pages to be compatible with all types of vocabularies, not just ones where
 * $vocab['module'] = taxonomy.
 */
function taxonomy_pages_link_alter(&$links, $node) {
  foreach ($links as $module => $link) {
    if (strstr($module, 'taxonomy_term')) {
      // Link back to the node and not the taxonomy term page
      $tid = arg(2, $links[$module]['href']);
      if (is_numeric($tid)) {
        $nid = taxonomy_pages_get_nid($tid);
        if ($nid) {
          $links[$module]['href'] = 'node/'. $nid;
        }
      }
    }
  }
}

/**
 * Implementation of hook_menu_alter().
 * Done to hijack any links that are not, by accident, altered.
 */
function taxonomy_pages_menu_alter(&$callbacks) {
  // Redirect to taxonomy pages page.
  $callbacks['taxonomy/term/%']['page callback'] = 'taxonomy_pages_redirect';
}

/**
 * Menu hijack.
 */
function taxonomy_pages_redirect($tid) {
  $nid = taxonomy_pages_get_nid($tid);
  if (!empty($nid)) {
    drupal_goto('node/'. $nid);
  }
  $term = taxonomy_get_term($tid);
  if (empty($term)) {
    return drupal_not_found();
  }
  $action = variable_get('taxonomy_pages_taxonomy_term_action', TAXONOMY_PAGES_NO_ACTION);
  switch ($action) {
    case TAXONOMY_PAGES_NO_ACTION:
      return taxonomy_term_page($tid);
    case TAXONOMY_PAGES_PAGE_NOT_FOUND:
      return drupal_not_found();
    case TAXONOMY_PAGES_CREATION_LINK:
      return theme('taxonomy_pages_creation_link', $term);
  }
}

/**
 * Implementation of hook_nodeapi
 */
function taxonomy_pages_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'load':
      $values['taxonomy_pages'] = taxonomy_pages_get_tid($node->nid);
      return $values;

    case 'presave':
      if (!empty($node->taxonomy_pages)) {
        $node->taxonomy_pages = taxonomy_pages_extract_tid($node->taxonomy_pages);
      }
      break;

    case 'update':
      if (!empty($node->taxonomy_pages)) {
        $update = array('nid' => $node->nid, 'tid' => $node->taxonomy_pages);
        print $node->taxonomy_pages;
        $tid = taxonomy_pages_get_tid($node->nid);
        if (empty($tid)) {
          drupal_write_record('taxonomy_pages', $update);
        }
        else {
          drupal_write_record('taxonomy_pages', $update, 'nid');
        }
      }
      elseif ($tid = taxonomy_pages_get_tid($node->nid)) {
        db_query('DELETE FROM {taxonomy_pages} WHERE nid = %d', $node->nid);
      }
      break;

    case 'insert':
      if (!empty($node->taxonomy_pages)) {
        $insert = array('nid' => $nide->nid, 'tid' => $node->taxonomy_pages);
        drupal_write_record('taxonomy_pages', $insert);
      }
      break;

    case 'delete':
      if (!empty($node->taxonomy_pages)) {
        db_query('DELETE FROM {taxonomy_pages} WHERE nid = %d', $node->nid);
      }
      break;

    case 'validate':
      if (!empty($node->taxonomy_pages)) {
        taxonomy_pages_node_validate($node);
      }
      break;

    case 'view':
      if (!empty($node->taxonomy_pages)) {
        $node->content['taxonomy_pages'] = array(
          '#value' => theme('taxonomy_pages_page', $node->taxonomy_pages),
          // We want it to "sink" all the way to the bottom, in order to display the node first.
          '#weight' => 254,
          '#type' => 'markup',
        );
        if (user_access('administer taxonomy')) {
          // Display a link to editing the term.
          $node->content['taxonomy_pages_admin_link'] = array(
            '#value' => l(t('Edit corresponding term'), 'admin/content/taxonomy/edit/term/'. $node->taxonomy_pages),
            '#weight' => 255,
          );
        }
      }
      break;
  }
}

/**
 * Node validation.
 */
function taxonomy_pages_node_validate($node) {
  $pages = 0;
  foreach ($node->taxonomy_pages as $tid) {
    if (!empty($tid)) {
      $pages++;
    }
  }
  if ($pages > 1) {
    form_set_error('taxonomy_pages', t('Content may only be taxonomy pages for one term.'));
  }
  $real = taxonomy_pages_extract_tid($node->taxonomy_pages);
  if (taxonomy_pages_get_nid($real) !== $node->nid && !empty($real)) {
    // This will only happen if two users attempt to create taxonomy pages for the same term at the same time.
    form_set_error('taxonomy_pages', t('A taxonomy page has already been created for the term you selected.'));
  }
}

/**
 * Extract the tid.
 */
function taxonomy_pages_extract_tid($taxonomy_pages, $return = FALSE) {
  static $curvid;
  if ($return) {
    return $curvid;
  }
  foreach ($taxonomy_pages as $vid => $tid) {
    if (!empty($tid)) {
      $curvid = $vid;
      return $tid;
    }
  }
}

/**
 * Implementation of hook_theme().
 */
function taxonomy_pages_theme() {
  return array(
    'taxonomy_pages_page' => array(
      'arguments' => array('tid' => NULL),
      'template' => 'taxonomy-pages-page',
    ),
    'taxonomy_pages_creation_link' => array(
      'arguments' => array('tid' => NULL, 'query' => NULL, 'termname' => NULL),
    ),
  );
}

/**
 * @ingroup Themeable
 */
function template_preprocess_taxonomy_pages_page(&$vars) {
  $type = variable_get('taxonomy_pages_display_type', TAXONOMY_PAGES_DEFAULT_DISPLAY);
  $vars['type'] = $type;
  $vars['term'] = taxonomy_get_term($vars['tid']);
  switch ($type) {
    case TAXONOMY_PAGES_DEFAULT_DISPLAY:
      $result = db_query("SELECT n.*, u.*, r.* FROM {term_node} t INNER JOIN {node} n ON t.vid = n.vid INNER JOIN {users} u ON u.uid = n.uid INNER JOIN {node_revisions} r ON r.vid = n.vid WHERE t.tid = '%d'", $vars['tid']);
      $nodes = array();
      $first = TRUE;
      while ($node = db_fetch_object($result)) {
        // Make sure the user has access to view the node in question.
        if (node_access('view', $node)) {
          $node->build_mode = NODE_BUILD_NORMAL;
          $nodes[] = theme('node', $node, TRUE);
        }
      }
      $vars['nodes'] = $nodes;
      break;
    case TAXONOMY_PAGES_TAXONOMY_TERM_DISPLAY:
      require_once(drupal_get_path('module', 'taxonomy') .'/taxonomy.admin.inc');
      $vars['output'] = menu_execute_active_handler(taxonomy_term_path($vars['term']));
      break;
  }
}

/**
 * @ingroup Themeable
 */
function theme_taxonomy_pages_creation_link($term = NULL, $query = NULL, $termname = NULL) {
  if (is_null($termname)) {
    $termname = $term->name;
  }
  drupal_set_title(t('Create new category page for the %term category', array('%term' => $termname)));
  $types = node_get_types();
  $typecount = 0;
  $output = '';
  foreach ($types as $type) {
    if (user_access('assign '. $type->name .' nodes to terms')) {
      $typecount++;
      if (is_null($query)) {
        $query = array('taxonomy_pages' => $term->tid);
      }
      $output .= '<dt><a href="'. url('node/add/'. str_replace('-', '_', $type->type), $query) .'">'. $type->name .'</a></dt><dd>'. $type->description .'</dd>';
    }
  }
  if (empty($typecount)) {
    // User does not have access to create any types.
    return drupal_access_denied();
  }
  else {
    if ($typecount == 1) {
      $output = t('<p></p><p><strong>There is currently no category page for the %term category. You may <em>create it</em> as a:</strong></p><p></p>', array('%term' => $termname)) . $output;
    }
    else {
      $output = t('<p></p><p><strong>There is currently no category page for the %term category. You may <em>create it</em> as one of the following:</strong></p><p></p>', array('%term' => $termname)) . $output;
    }
  }
  return $output;
}